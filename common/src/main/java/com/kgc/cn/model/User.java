package com.kgc.cn.model;

import java.io.Serializable;

/**
 * Created by boot on 2019/11/27.
 */
public class User implements Serializable {
    private int id;
    private String userName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
