package com.kgc.cn.service;

import com.kgc.cn.vo.GoodsVo;

import java.util.List;

/**
 * Created by boot on 2019/11/30.
 */
public interface GoodsService {

    List<GoodsVo> show(GoodsVo goods);
}
