package com.kgc.cn.service;

import com.kgc.cn.vo.MqTestVo;

/**
 * Created by boot on 2019/12/5.
 */
public interface ActiveService {
    /**
     * 点对点的操作方式
     *
     * @param name
     * @param mqTestVo
     */
    void sendMsgByQueue(String name, MqTestVo mqTestVo);

    /**
     * 订阅操作方式
     * @param name
     * @param mqTestVo
     */
    void sendMsgByTopic(String name, MqTestVo mqTestVo);

}
