package com.kgc.cn.service;

import com.kgc.cn.model.User;
import com.kgc.cn.vo.UserLoginVo;

/**
 * Created by boot on 2019/11/27.
 */
public interface UserServce {
    User getUser();

    /**
     * 登录方法
     * @param userLoginVo
     * @return
     */
    String userLogin(UserLoginVo userLoginVo);
}
