package com.kgc.cn.vo;

/**
 * Created by boot on 2019/11/30.
 */

public class UserLoginVo {
    private String phone;
    private String password;
    private WxUserVo wxUserVo;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
