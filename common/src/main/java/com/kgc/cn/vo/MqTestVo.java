package com.kgc.cn.vo;

import java.io.Serializable;

/**
 * Created by boot on 2019/12/5.
 */

public class MqTestVo implements Serializable {
    private int id;
    private String name;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MqTestVo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
