package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.kgc.cn.dto.GoodsDto;
import com.kgc.cn.mapper.GoodsMapper;
import com.kgc.cn.model.GoodsExample;
import com.kgc.cn.vo.GoodsVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by boot on 2019/11/30.
 */
@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public List<GoodsVo> show(GoodsVo goodsVo) {
        GoodsDto goodsDto = new GoodsDto();
        BeanUtils.copyProperties(goodsVo, goodsDto);
        List<GoodsDto> goodsDtoList = goodsMapper.show(goodsDto);
        List<GoodsVo> goodsVoList = Lists.newArrayList();
        goodsDtoList.forEach(goodsDtos -> {
            GoodsVo goodsVos = new GoodsVo();
            BeanUtils.copyProperties(goodsDtos, goodsVos);
            goodsVoList.add(goodsVo);
        });
        return goodsVoList;
    }

}
