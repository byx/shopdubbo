package com.kgc.cn.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.mqDto.MqTest;
import com.kgc.cn.utils.ActiveMqUtils;
import com.kgc.cn.vo.MqTestVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

/**
 * Created by boot on 2019/12/5.
 */
@Service
@Log4j
public class ActiveServiceImpl implements ActiveService {
    @Autowired
    private ActiveMqUtils activeMqUtils;


    @Override
    public void sendMsgByQueue(String name, MqTestVo mqTestVo) {
        MqTest mqTest = new MqTest();
        BeanUtils.copyProperties(mqTestVo, mqTest);
        activeMqUtils.sendMsgByQueue(name, mqTest);
    }

    @Override
    public void sendMsgByTopic(String name, MqTestVo mqTestVo) {
        MqTest mqTest = new MqTest();
        BeanUtils.copyProperties(mqTestVo, mqTest);
        activeMqUtils.sendMsgByTopic(name, mqTest);
    }

    @JmsListener(destination = "test-queue")
    private void listenerByQueue(MqTest mqTest) {
        log.info(mqTest);
    }

    @JmsListener(destination = "test-topic")
    private void listenerByTopic(MqTest mqTest) {
        log.info(mqTest);
    }

    @JmsListener(destination = "test-topic")
    private void listenerByTopic1(MqTest mqTest) {
        log.info(mqTest);
    }
    @JmsListener(destination = "test-topic")
    private void listenerByTopic2(MqTest mqTest) {
        log.info(mqTest);
    }
}
