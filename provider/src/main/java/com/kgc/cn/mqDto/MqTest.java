package com.kgc.cn.mqDto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by boot on 2019/12/5.
 */
@Data
@ToString
public class MqTest implements Serializable {
    private int id;
    private String name;
}
