package com.kgc.cn.utils;

import com.kgc.cn.mqDto.MqTest;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;
import javax.jms.Topic;

/**
 * Created by boot on 2019/12/5.
 */
@Component
public class ActiveMqUtils {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    /**
     * 点对点
     * @param name
     * @param o
     */
    public void sendMsgByQueue(String name, Object o) {
        Queue queue = new ActiveMQQueue(name);
        jmsMessagingTemplate.convertAndSend(queue, o);
    }

    /**
     * 订阅
     * @param name
     * @param o
     */
    public void sendMsgByTopic(String name, Object o) {
        Topic topic = new ActiveMQTopic(name);
        jmsMessagingTemplate.convertAndSend(topic, o);
    }
}
