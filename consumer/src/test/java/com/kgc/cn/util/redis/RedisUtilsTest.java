package com.kgc.cn.util.redis;

import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by boot on 2019/12/6.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j
public class RedisUtilsTest {
    @Autowired
    private RedisUtils redisUtils;

    @Test
    public void checkSpeed() throws Exception {
        boolean result = redisUtils.checkSpeed("tests", 1, 10);
        log.info(result);
    }

}