package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.UUID;

/**
 * Created by boot on 2019/12/3.
 */
@ApiModel
@Data
public class GoodsParam {
    @ApiModelProperty("商品id")
    private String gId;
    @ApiModelProperty("商品名")
    private String gName;
    @ApiModelProperty("商品价格")
    private Double gPrice;
    @ApiModelProperty("商品类型")
    private String gProperty;
}
