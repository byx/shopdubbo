package com.kgc.cn.util.wx;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by boot on 2019/12/2.
 */
@Component
@ConfigurationProperties(prefix = "wx")
@Data
public class WxUtils {
    private String appId;
    private String redirectUri;
    private String scope;
    private String codeUrl;
    private String accessTokenUrl;
    private String secret;
    private String userInfoUrl;

    public String reqCode() throws UnsupportedEncodingException {
        StringBuffer sb = new StringBuffer(getCodeUrl());
        sb.append("appid=").append(getAppId());
        sb.append("&").append("redirect_uri=").append(URLEncoder.encode(getRedirectUri(), "UTF-8"));
        sb.append("&").append("response_type=").append("code");
        sb.append("&").append("scope=").append(getScope());
        sb.append("&").append("state=").append("STATE");
        sb.append("#wechat_redirect");
        return sb.toString();
    }

    public String reqAccessToken(String code) {
        StringBuffer sb = new StringBuffer(getAccessTokenUrl());
        sb.append("appid=").append(getAppId());
        sb.append("&").append("secret=").append(getSecret());
        sb.append("&").append("code=").append(code);
        sb.append("&").append("grant_type=").append("authorization_code");
        return sb.toString();
    }

    public String reqUserInfo(String accessToken, String openId) {
        StringBuffer sb = new StringBuffer(getUserInfoUrl());
        sb.append("access_token=").append(accessToken);
        sb.append("&").append("openid=").append(openId);
        sb.append("&").append("lang=").append("zh_CN");
        return sb.toString();
    }

}
