package com.kgc.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.service.ActiveService;
import com.kgc.cn.vo.MqTestVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by boot on 2019/12/5.
 */
@Api(tags = "active")
@RestController
@RequestMapping(value = "/active")
public class ActiveMQController {
    @Reference
    private ActiveService activeService;

    @ApiOperation("发送点对点消息")
    @GetMapping(value = "/sendMsgByQueue")
    public void sendMsgByQueue(@Valid MqTestVo mqTestVo) {
    }

    @ApiOperation("发送发布订阅消息")
    @GetMapping(value = "/sendMsgByTopic")
    public void sendMsgByTopic(@Valid MqTestVo mqTestVo) {
    }
}
