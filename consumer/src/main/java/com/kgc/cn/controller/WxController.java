package com.kgc.cn.controller;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.util.client.UrlUtils;
import com.kgc.cn.util.redis.RedisUtils;
import com.kgc.cn.util.wx.WxUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;


/**
 * Created by boot on 2019/12/2.
 */
@Controller
@RequestMapping(value = "wx")
public class WxController {
    @Autowired
    private WxUtils wxUtils;
    @Autowired
    private RedisUtils redisUtils;

    @GetMapping(value = "/toLogin")
    public String toLogin() throws Exception {
        return "redirect:" + wxUtils.reqCode();
    }

    @GetMapping("callBack")
    public String callBack(String code) throws Exception {
        //第二步通过第一部的code换取accessToken与openId
        //请求get
        String jsonStr = UrlUtils.loadURL(wxUtils.reqAccessToken(code));
        JSONObject jsonObject = JSONObject.parseObject(jsonStr);
        String accessToken = jsonObject.getString("access_token");
        String openId = jsonObject.getString("openid");
        String userInfoJsonStr = UrlUtils.loadURL(wxUtils.reqUserInfo(accessToken, openId));
        redisUtils.set(openId, userInfoJsonStr);
        return null;
    }

}
