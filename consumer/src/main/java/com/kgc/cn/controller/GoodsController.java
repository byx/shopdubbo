package com.kgc.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.service.GoodsService;
import com.kgc.cn.util.result.ReturnResult;
import com.kgc.cn.util.result.ReturnResultUtils;
import com.kgc.cn.vo.GoodsVo;
import com.kgc.cn.param.GoodsParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by boot on 2019/12/3.
 */
@Api(tags = "商品")
@RestController
@RequestMapping(value = "/goods")
public class GoodsController {
    @Reference
    private GoodsService goodsService;

    @ApiOperation("查询商品")
    @PostMapping(value = "/show")
    public ReturnResult<List<GoodsVo>> show(@RequestBody GoodsParam goodsParam) {
        GoodsVo goodsVo = new GoodsVo();
        BeanUtils.copyProperties(goodsParam, goodsVo);
        List<GoodsVo> goodsVoList = goodsService.show(goodsVo);
        return ReturnResultUtils.returnSuccess(goodsVoList);
    }

    @PostMapping(value = "/saveGoods")
    public ReturnResult saveGoods(@RequestBody GoodsParam goodsParam) {

        //todo 通过商品类型将商品分类型存储
        return ReturnResultUtils.returnSuccess();
    }

    @PostMapping(value = "/save")
    public ReturnResult save(@RequestBody GoodsParam goodsParam) {

        //todo 通过商品类型将商品分类型存储
        return ReturnResultUtils.returnSuccess();
    }
}
