package com.kgc.cn.controller;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.config.aop.CurrentUser;
import com.kgc.cn.config.aop.LoginRequired;
import com.kgc.cn.model.User;
import com.kgc.cn.service.GoodsService;
import com.kgc.cn.service.UserServce;
import com.kgc.cn.util.redis.RedisUtils;
import com.kgc.cn.util.result.ReturnResult;
import com.kgc.cn.util.result.ReturnResultUtils;
import com.kgc.cn.vo.UserLoginVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by boot on 2019/11/27.
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private RedisUtils redisUtils;
    @Reference
    private UserServce userServce;
    @Reference
    private GoodsService goodsService;

    @GetMapping(value = "/get")
    public User saveUser() {
        return userServce.getUser();
    }


    @PostMapping(value = "/supportLogin")
    public String toLogin(HttpServletRequest request) {
        //todo 验证账号密码
        String token = request.getSession().getId();
        //todo 使用redis 保存用户信息 key为token value为userJsonStr 并设置过期时间
        User user = new User();
        user.setId(1);
        user.setUserName("syGroup");
        redisUtils.set(token, JSONObject.toJSONString(user));
        return token;
    }

    @LoginRequired
    @GetMapping(value = "/pay")
    public boolean isPay(@CurrentUser User user) {
        //todo 是谁要pay
        return true;
    }

    @ApiOperation("登录")
    @PostMapping(value = "/toLogin")
    public ReturnResult<String> toLogin(@Valid UserLoginVo userLoginVo, HttpServletRequest request) {
        String isLoginUser = userServce.userLogin(userLoginVo);
        String token = null;
        if (StringUtils.isNotEmpty(isLoginUser)) {
            token = request.getSession().getId();
            redisUtils.set(token, isLoginUser);
        }
        return ReturnResultUtils.returnSuccess(token);
    }


    /**
     * 逻辑删除 与 物理删除
     * 更新        delete
     * isDel
     * 0，1
     *
     * 注销用户登录
     *
     */

}
