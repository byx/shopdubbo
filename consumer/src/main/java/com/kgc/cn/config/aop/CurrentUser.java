package com.kgc.cn.config.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by boot on 2019/11/28.
 */
@Target(ElementType.PARAMETER)//放在方法参数中使用
@Retention(RetentionPolicy.RUNTIME)
public @interface CurrentUser {
}
